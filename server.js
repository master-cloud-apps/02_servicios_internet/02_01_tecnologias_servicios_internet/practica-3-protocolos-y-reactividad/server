const { createApp } = require('./src/app/index.js')
const { sequelize } = require('./src/db/sequelize.js')
const { createChannel, handlers, closeConnection } = require('./src/queue/index')
const EventEmitter = require('events')

const eventEmitter = new EventEmitter()
const websockets = {}

sequelize().sync({ force: true })
  .then(() => createChannel(process.env.AMQP_URL))
  .then(params => Promise.resolve(handlers({ channel: params.channel, eventEmitter }))
    .then(queueHandler => queueHandler.consumePlantCreation())
    .then(() => createApp({ websockets, eventEmitter }).listen(3000, () => console.log('Listening on 3000')))
    .catch(error => {
      console.error(error)
      closeConnection(params.conn)
    }))
  .catch(error => console.error(error))
