const { websocket } = require('./../endpoints')
const express = require('express')

const routerHandler = ({ eventEmitter, websockets }) => {
  const router = new express.Router()
  const webSocketsHanlder = websocket({ eventEmitter, websockets })

  router.ws('/notifications', webSocketsHanlder.registerUser)

  return router
}

module.exports = routerHandler
