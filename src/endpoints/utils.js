const manageChainError = (error, response) => {
  if (error.statusCode === undefined) {
    return response.status(500).send(error)
  }
}

module.exports = {
  manageChainError
}
