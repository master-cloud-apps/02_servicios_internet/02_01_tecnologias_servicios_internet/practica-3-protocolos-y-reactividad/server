const { Eoloplant } = require('./../models/eoloplant')
const { manageChainError } = require('./utils.js')

const handlers = ({ eventEmitter }) => ({
  post: async (req, res) => {
    const userId = req.header('userId')
    const createPlant = () => ({ city: req.body.city, progress: 0, completed: false, planning: null, userId })
    const parseValidationError = error => error.name === 'SequelizeValidationError' ? Promise.reject(res.status(400).send(error)) : Promise.reject(error)
    const insertPlant = eoloplant => Eoloplant.create(eoloplant).catch(error => parseValidationError(error))

    return Promise.resolve(createPlant())
      .then(eoloplant => insertPlant(eoloplant))
      .then(eoloplant => {
        eventEmitter.emit('plant_progress', JSON.stringify(eoloplant), userId)
        eventEmitter.emit('plant_creation', JSON.stringify({ id: eoloplant.id, city: eoloplant.city }))
        return eoloplant
      })
      .then(plant => res.status(200).send(plant))
      .catch(error => manageChainError(error, res))
  },
  getCityById: async (req, res) => {
    const eoloplant = await Eoloplant.findByPk(req.params.cityId)
    if (eoloplant === null) {
      return res.sendStatus(404)
    }
    return res.status(200).send(eoloplant)
  },
  getEoloplants: (req, res) => {
    return Eoloplant.findAll()
      .then(eoloplants => res.status(200).send(eoloplants))
  },
  deleteEoloplantById: (req, res) => {
    return Eoloplant.findByPk(req.params.cityId)
      .then(eoloplant => {
        if (eoloplant === null) {
          return res.sendStatus(404)
        }
        return eoloplant.destroy().then(() => res.sendStatus(204))
      })
  }
})

module.exports = handlers
