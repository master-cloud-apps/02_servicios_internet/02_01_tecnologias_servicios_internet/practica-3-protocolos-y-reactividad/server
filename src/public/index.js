// eslint-disable-next-line no-unused-vars
const utils = (function () {
  let userId
  let eoloplant

  function getEoloplants () {
    return fetch(`http://${window.location.host}/api/eoloplants`, {
      method: 'GET'
    }).then(response => response.json())
  }

  function createPlant (cityName) {
    const data = {
      city: cityName
    }
    fetch('http://localhost:3000/api/eoloplants', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        userId: userId
      },
      body: JSON.stringify(data)
    }).then(response => response.json())
      .then(eoloplantData => {
        eoloplant = eoloplantData
        console.log(eoloplant)
      })
  }

  const getEoloplantDescription = plant => `${plant.city}: Progress --> ${plant.progress} Planning --> ${plant.planning}`
  const setUserId = (externalUserId) => (userId = externalUserId)

  return {
    createPlant,
    getEoloplants,
    getEoloplantDescription,
    setUserId
  }
})()
