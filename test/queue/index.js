const amqp = require('amqplib')

const createChannel = () => {
  return amqp.connect('amqp://localhost')
    .then(conn => conn.createChannel().then(channel => ({ conn, channel })))
}

const closeConnection = (connection) => {
  return connection.close()
}

module.exports = {
  createChannel, closeConnection
}
