const { expect } = require('chai')
const { app } = require('./../../../src/app/index.js')
const request = require('supertest')

describe('Use cases for the public folder', () => {
  it('When call the root edpoint should return the html', () => {
    return request(app).get('/').then(response => {
      expect(response.type).to.be.equal('text/html')
      expect(response.statusCode).to.be.equal(200)
    })
  })
})
